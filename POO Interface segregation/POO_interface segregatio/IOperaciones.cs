﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    interface IOperaciones:Icomputadora2, Icomputadora3
    {
        void Presentacion(); // un método general que todos las clases necesita
    }
}
