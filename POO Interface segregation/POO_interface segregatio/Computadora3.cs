﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    class Computadora3 :Icomputadora3 //se implementa la interfaz que se necesita que es la computadora 3
    {
        // se utilizan los metodos que solo necesita que en este caso son 2
        public void Presentacion()
        {
            Console.WriteLine("La computadora HP:");
         }      
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }

    }
}
