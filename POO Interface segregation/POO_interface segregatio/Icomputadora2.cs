using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    interface Icomputadora2 //interfaz con los metodos que solo necesita la computadora 2
    {
        void Suma();
        void Resta();
    }
}
