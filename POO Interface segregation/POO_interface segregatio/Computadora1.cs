﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    class Computadora1:IOperaciones
    {

        // se utilizan los metodos que solo necesita que en este caso son todos 
        public void Presentacion()
        {
            Console.WriteLine("La computadora DEll: ");
        }
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }

        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }
    }
}
