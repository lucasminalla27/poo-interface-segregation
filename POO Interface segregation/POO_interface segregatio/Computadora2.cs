﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    class Computadora2 : Icomputadora2 //se implementa la interfaz que se necesita que es la computadorea 2
    {
        // se utilizan los metodos que solo necesita que en este caso son 3
        public void Presentacion()
        {
            Console.WriteLine("La computadora LENOVO:");
        }
        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }

    }
}
