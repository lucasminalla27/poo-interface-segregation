﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    class Computadora2:IOperaciones
    {   //metodoss a utlizar
        public void Presentacion()
        {
            Console.WriteLine("La computadora HP: ");
        }
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }

        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }

    }
}

