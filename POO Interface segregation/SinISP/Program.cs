﻿using System;
namespace POO_interface_segregation
   
{
    class Program
    {
        static void Main(string[] args)
        {
            Computadora1 computadora1 = new Computadora1();
            computadora1.Presentacion();
            computadora1.Division();
            computadora1.Resta();
            computadora1.Suma();

            Console.WriteLine("--------------------");
            Computadora2 computadora2 = new Computadora2();
            computadora2.Presentacion();
            computadora2.Division();
            computadora2.Resta();
            computadora2.Suma();

            Console.WriteLine("---------------------");
            Computadora3 computadora3 = new Computadora3();
            computadora3.Presentacion();
            computadora3.Division();
            computadora3.Resta();
            computadora3.Suma();
            


            Console.ReadKey();
        }
    }
}
