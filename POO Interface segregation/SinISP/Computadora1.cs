﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    class Computadora1:IOperaciones
    {

        //Métodos a utilizar 
        public void Presentacion()
        {
            Console.WriteLine("La computadora LENOVO: ");
        }
        public void Division()
        {
            Console.WriteLine("Puede realizar una división");
        }

        public void Resta()
        {
            Console.WriteLine("Puede realizar una resta");
        }

        public void Suma()
        {
            Console.WriteLine("Puede realizar una suma");
        }

    }
}
