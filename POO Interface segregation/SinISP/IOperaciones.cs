﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POO_interface_segregation
{
    interface IOperaciones
    {    
        // metodos a usar en todas las computadoreas aunque no sean necesarios

        void Division();
        void Suma();
        void Resta();
        void Presentacion();
    }
}
